FROM node:8.9.3

# Install Java
RUN echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y -t jessie-backports openjdk-8-jre

# Install Chrome
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install

RUN mkdir -p /usr/app/protractor

WORKDIR /usr/app/protractor

COPY . .

RUN npm install

CMD npm run start & sleep 10 && npm run update && npm run e2e
