# Challenge - Tulio Luz

## Technologies used

I used the technologies below:

### end to end Testing

* [Protractor](https://www.protractortest.org/#/): Protractor is an end-to-end test framework for Angular and AngularJS applications. Protractor runs tests against your application running in a real browser, interacting with it as a user would;
* [Page Objects](https://www.protractortest.org/#/page-objects): Page Objects help you write cleaner tests by encapsulating information about the elements on your application page. A Page Object can be reused across multiple tests, and if the template of your application changes, you only need to update the Page Object;
* [CucumberJS](https://github.com/cucumber/cucumber-js): Cucumber is a tool for running automated tests written in plain language. Because they're written in plain language, they can be read by anyone on your team. Because they can be read by anyone, you can use them to help improve communication, collaboration and trust on your team;

### Environment

* [Docker](https://www.docker.com/): Docker provides container software that is ideal for developers and teams looking to get started and experimenting with container-based applications.

## Folders Structures

* ```e2e ```
    * ```features ``` Where feature files should be created
        * ```sortTable.feature ```
    * ```pages ``` Where the page object of tests should be created
        * ```sortTable.page.js ```
    * ```specs ``` Where the specification of tests should be created
        * ```sortTable.spec.js ```
* ```Dockerfile``` has the responsibility to up the server and run the tests in the same container
* ```protractor.conf.js``` This configuration tells Protractor where your test files (specs) are, and where to talk to your Selenium Server (seleniumAddress).

## Set up

I'm using a single container to run the application and the e2e testing.

### Requirement

- Install [Docker](https://docs.docker.com/install/)

## Running the tests with Docker locally

- Build the image docker ```docker build . -t e2e```;

- Run the tests with docker ```docker run e2e```

## Pipeline

I set up the pipeline using just one step to run the application and e2e testing automation, there is a cache to the build step, it provides more speed when it is running.

There is a validor to check your yml file: https://bitbucket-pipelines.atlassian.io/validator

_______________________________________________________

# Welcome to Web Testing Challenge
![enter image description here](https://image.ibb.co/bXHOB8/rsz_minders_stickers_05.png)

Hello fellow candidate! :wave:

As a QA Engineer your task is write beautiful automated tests and spot some bugs. To do that, on this repo you will find a very simple App that we are going to use as the target for our tests.

# Challenge

### Install and Run
First let's run the web app to see how it's look like:
```
npm install -g parcel
```
```
npm install
```
```
npm start
```
Now you must be able to see the web app on http://localhost:3000

##

### Rules and Goal
As you can see the app is pretty simple, and it just contains a sortable table where the rows are randomly numerated from 0 to 5.

The goal of this challenge is to sort the rows of this table by drag and drop them from the lowest value to the top .

You can use the tools, frameworks and programming languages you want to do this challenge.

As a talented QAE you are also responsible by integrating the tests in CI, to do that we must have our project wrapped in a container. Use docker container to run your scripts

Write a Readme file where you must detail all the steps needed to run your script.


**Good luck and have fun! :)**