let sortTable = require('../pages/sortTable.page.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1200);

Given('The user is on the table page', async function () {
    await browser.waitForAngularEnabled(false);
    await sortTable.getTablePage();
});

When('The user drag and drop the itens on the table', async function () {
    await sortTable.orderTable();
});

Then('The user should see the table by order increase', async function () {
    expect(await sortTable.getItem0())
        .to.equal("Item 0");
    expect(await sortTable.getItem1())
        .to.equal("Item 1");
    expect(await sortTable.getItem2())
        .to.equal("Item 2");
    expect(await sortTable.getItem3())
        .to.equal("Item 3");
    expect(await sortTable.getItem4())
        .to.equal("Item 4");
    expect(await sortTable.getItem5())
        .to.equal("Item 5");
});