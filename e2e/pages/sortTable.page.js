let SortTable = function () {
    var i;
    var lengthTable;

    let allItem = $$('#app ul li');

    this.getTablePage = async function () {
        await browser.get("/");
    };

    this.orderTable = async function () {
        lengthTable = (await allItem).length;

        for (i = 0; i < lengthTable; i++) {
            let item = element(by.xpath("//*[@id='app']/ul/li[contains(text(),'Item " + i + "')]"));
            await browser.actions().dragAndDrop(item, { x: 0, y: 300 }).perform();
        }
    };

    this.getItem0 = function () {
        return allItem.get(0).getText();
    }
    this.getItem1 = function () {
        return allItem.get(1).getText();
    }
    this.getItem2 = function () {
        return allItem.get(2).getText();
    }
    this.getItem3 = function () {
        return allItem.get(3).getText();
    }
    this.getItem4 = function () {
        return allItem.get(4).getText();
    }
    this.getItem5 = function () {
        return allItem.get(5).getText();
    }
}
module.exports = new SortTable();